﻿using System;
using System.Windows.Forms;

namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(5000);
            var aString = txt01.Text.Trim();
            var aLines = aString.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);
            var aMS = int.Parse(textBox1.Text.Trim());
            var firstRun = false;
            while (checkBox1.Checked || firstRun == false)
            {
                firstRun = true;

                foreach (var aLin in aLines)
                {
                    if (string.IsNullOrEmpty(aLin))
                        continue;
                    Application.DoEvents();
                    foreach (var aHuruf in aLin)
                    {
                        if (aHuruf == '(')
                            SendKeys.Send("{(}");
                        else if (aHuruf == ')')
                            SendKeys.Send("{)}");
                        else
                            SendKeys.Send($"{aHuruf}");

                        Application.DoEvents();
                        System.Threading.Thread.Sleep(aMS);
                    }
                    SendKeys.Send("{ENTER}");
                }

            }
        }
    }
}
