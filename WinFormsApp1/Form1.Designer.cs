﻿
namespace WinFormsApp1
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            button1 = new System.Windows.Forms.Button();
            txt01 = new System.Windows.Forms.TextBox();
            textBox1 = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            checkBox1 = new System.Windows.Forms.CheckBox();
            SuspendLayout();
            // 
            // button1
            // 
            button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            button1.Location = new System.Drawing.Point(12, 431);
            button1.Name = "button1";
            button1.Size = new System.Drawing.Size(206, 23);
            button1.TabIndex = 0;
            button1.Text = "Start Simulate Type";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // txt01
            // 
            txt01.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            txt01.Location = new System.Drawing.Point(12, 34);
            txt01.Multiline = true;
            txt01.Name = "txt01";
            txt01.Size = new System.Drawing.Size(641, 391);
            txt01.TabIndex = 1;
            txt01.Text = resources.GetString("txt01.Text");
            // 
            // textBox1
            // 
            textBox1.Location = new System.Drawing.Point(208, 1);
            textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            textBox1.Name = "textBox1";
            textBox1.Size = new System.Drawing.Size(78, 23);
            textBox1.TabIndex = 2;
            textBox1.Text = "500";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(12, 9);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(190, 15);
            label2.TabIndex = 4;
            label2.Text = "Delay per character in milliseconds";
            // 
            // checkBox1
            // 
            checkBox1.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            checkBox1.AutoSize = true;
            checkBox1.Checked = true;
            checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            checkBox1.Location = new System.Drawing.Point(237, 434);
            checkBox1.Name = "checkBox1";
            checkBox1.Size = new System.Drawing.Size(61, 19);
            checkBox1.TabIndex = 6;
            checkBox1.Text = "Loop ?";
            checkBox1.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(665, 464);
            Controls.Add(checkBox1);
            Controls.Add(textBox1);
            Controls.Add(txt01);
            Controls.Add(button1);
            Controls.Add(label2);
            Name = "Form1";
            Text = "Jomiuns Simulate Typing";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txt01;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}

